//
//  data.swift
//  PPMagazines
//
//  Created by iMac on 17.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class Magazine {
    //class construction
    var id: Int
    var title: String
    var product_id: String
    var is_free: Bool
    //var published_at: Date
    var has_bought: Bool
    var has_download: Bool
    var cover_large: String
    var cover_medium: String
    var has_demo: Bool
    
    //init data function
    init(id: Int,
         title: String,
         product_id: String,
         is_free: Bool,
         /*published_at: Date,*/
        has_bought: Bool,
        has_download: Bool,
        cover_large: String,
        cover_medium: String,
        has_demo: Bool) {
        
        self.id = id
        self.title = title
        self.product_id = product_id
        self.is_free = is_free
        //self.published_at = published_at
        self.has_bought = has_bought
        self.has_download = has_download
        self.cover_large = cover_large
        self.cover_medium = cover_medium
        self.has_demo = has_demo
    }
    
    //temporary data - w przyszlosci sciagane z serwera  (realm swift)
    static func fetchMagazines() -> [Magazine] {
        return [
            Magazine(id: 23392, title: "wwdsad", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: true, has_bought: false, has_download: false, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch3.presspadapp.com/cover_medium/6807b1b014144483ec5cdda932458573.jpg", has_demo: true),
            Magazine(id: 23393, title: "fsdfsdfs", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: false, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch1.presspadapp.com/cover_medium/0a30a20fa80d63280b423106f5d5f60b.jpg", has_demo: false),
            Magazine(id: 23394, title: "fsdfsf", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch2.presspadapp.com/cover_medium/eb2b2d03c0d46810ef62780d3bc1b3a9.jpg", has_demo: true),
            Magazine(id: 23395, title: "sdfsfsdf", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch1.presspadapp.com/cover_medium/6f77c2b182304a13cc7f491908523a5a.jpg", has_demo: false),
            Magazine(id: 23396, title: "HELVETICA", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch3.presspadapp.com/cover_medium/384c145b11dd8118e65dbdce124e821c.jpg", has_demo: true),
            Magazine(id: 23397, title: "PRESS PAD", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch3.presspadapp.com/cover_medium/3042da0f807a6003de3133f215dc3111.jpg", has_demo: true),
            Magazine(id: 23398, title: "#72", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch1.presspadapp.com/cover_medium/086b3d9826d0491c326ec3c4cc22c680.jpg", has_demo: true),
            Magazine(id: 23399, title: "#72", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch3.presspadapp.com/cover_medium/b794a03e5250fc0b1faf4d9d831179bb.jpg", has_demo: true),
            Magazine(id: 23400, title: "#72", product_id: "com.presspadapp.jackkirbycollector.issue76", is_free: false, has_bought: false, has_download: true, cover_large: "https://cdncch1.presspadapp.com/cover_large/6807b1b014144483ec5cdda932458573.jpg" , cover_medium: "https://cdncch1.presspadapp.com/cover_medium/db6ae837f426ea5d568a4fc10e08735b.jpg", has_demo: true)
        ]
    }
}


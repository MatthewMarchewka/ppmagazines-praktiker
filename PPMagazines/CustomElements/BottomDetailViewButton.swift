//
//  BottomButton.swift
//  PPMagazines
//
//  Created by iMac on 22.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class BottomDetailViewButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

}

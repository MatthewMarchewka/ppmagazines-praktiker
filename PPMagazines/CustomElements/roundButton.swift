//
//  CustomButton.swift
//  PPMagazines
//
//  Created by iMac on 18.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class roundButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
        self.imageEdgeInsets = UIEdgeInsetsMake(13.0, 13.0, 13.0, 13.0)
        self.contentVerticalAlignment = .fill
        self.contentHorizontalAlignment = .fill
    }
}

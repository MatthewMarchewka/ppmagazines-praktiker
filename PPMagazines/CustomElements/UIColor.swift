//
//  UIColor.swift
//  PPMagazines
//
//  Created by iMac on 24.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

extension UIColor {
    
    //static let transparentBlack = UIColor(hex: 0x000000, a: 0.5)
    //static let customRed = UIColor(red: 255, green: 0, blue: 0)
    
    //NAVIGATION BAR
    static let navigationBlue = UIColor(red: 67, green: 150, blue: 213)
    //BUTTONS
    static let downloadedGreen = UIColor(red: 69, green: 202, blue: 158)
    static let buyRed = UIColor(red: 204, green: 49, blue: 51)
    static let downloadBlue = UIColor(red: 54, green: 133, blue: 193)
    
    //RGB COLORS
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0){
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    //HEX COLORS
    convenience init(hex: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            a: a
        )
    }
    
}

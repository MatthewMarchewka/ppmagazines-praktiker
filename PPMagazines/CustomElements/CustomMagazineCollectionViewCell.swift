//
//  CustomMagazineCollectionViewCell.swift
//  PPMagazines
//
//  Created by iMac on 17.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class CustomMagazineCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var magazineStatus: UIButton!
    @IBOutlet weak var demoStatus: UIButton!

    var magazine: Magazine? {
        didSet {
            self.updateUI()
        }
    }

    private func updateUI(){
        featuredImageView.image = nil
        if let magazine = magazine {
            //cover photo
            featuredImageView.sd_setImage(with: URL(string: magazine.cover_medium))       
            //status button
            if(magazine.has_download){
                magazineStatus.backgroundColor = UIColor.downloadedGreen
                magazineStatus.setImage(#imageLiteral(resourceName: "check"), for: UIControlState.normal)
            }else if(!magazine.has_bought && !magazine.is_free){
                magazineStatus.backgroundColor = UIColor.buyRed
                magazineStatus.setImage(#imageLiteral(resourceName: "money"), for: UIControlState.normal)
            }else{
                magazineStatus.backgroundColor = UIColor.downloadBlue
                magazineStatus.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
            }
            //demo button
            demoStatus.isHidden = false
            if(!magazine.has_demo){
               demoStatus.isHidden = true
            }
//koniec podstawiania elementu -------------------------------------------------------
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: 0, height: 0)
    }
}



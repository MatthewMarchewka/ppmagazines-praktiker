//
//  MagazineViewController.swift
//  PPMagazines
//
//  Created by iMac on 17.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class MagazineViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var swipeToggle: UIView!
    @IBAction func onMenuTapped() {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    private let magazines  = Magazine.fetchMagazines()
    var columns: Int = 2      //declare variable containing number of columns depends on Screen size
    var showOffset: Double = 0.0   //zmienna odpowiedzialna za przesuniecie poczatku wyswietlania
    var conditionOffset: Bool = true    //warunek potrzebny by offset wykonal sie tylko raz
    
    override func viewDidLoad() {
        super.viewDidLoad()
        conditionOffset = true
        let size = UIScreen.main.bounds.size.height
        switch size {
        case 0..<600:
            columns = 1
        case 600..<1100:
            columns = 2
        default:
            columns = 3
        }
        collectionView?.dataSource = self
        collectionView?.delegate = self
        //gesture recognizer
        let openMenuSwipe = UISwipeGestureRecognizer(target: self, action: #selector(onMenuTapped))
        openMenuSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.swipeToggle.addGestureRecognizer(openMenuSwipe)
    }
    
    //powrót do wlasciwego elementu
    override func viewDidLayoutSubviews() {
        if(conditionOffset){
            //obliczamy szerokosc komorki
            let cellHeight = Int( Int(collectionView.frame.height) / columns) - Int(5 * (columns-1))     //odejmujemy 5 z powodu cienia i marginesow dolnych
            let cellWidth = Int(cellHeight * 2/3)
            //mnozymy szerokosc komorki razy indexelementu
            let numberOfRows = floor((showOffset/Double(columns)))
            var targetOffset = Double(cellWidth)*numberOfRows
            targetOffset = targetOffset + 10*numberOfRows      //dodajemy spacing pomiedzy komorkami
            //przenosimy widok na wybrany rzad
            self.collectionView.contentOffset = CGPoint(x: targetOffset, y: 0.0)
            //wylaczamy offset
            conditionOffset = false
        }
    }
    
    //klikanie na zdjecie magazynu
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        performSegue(withIdentifier: "detail", sender: indexPath.row)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "detail"){
            let navigationDetailController = segue.destination as? NavigationDetailController
            let detailController = navigationDetailController?.viewControllers.first as? DetailViewController
            detailController?.indexDetail = (sender as? Int)!
        }
    }
}

//okresla ilosc danych i czym sa wypelniane
extension MagazineViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return magazines.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! CustomMagazineCollectionViewCell
        cell.magazine = magazines[indexPath.item]
        return cell
    }
}

//określanie wielkosci elementow
extension MagazineViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //proporcja 1,5 : 1
        let cellHeight = Int( Int(collectionView.frame.height) / columns) - Int(5 * (columns-1))     //odejmujemy 5 z powodu cienia i marginesow dolnych
        let cellWidth = Int(cellHeight * 2/3)
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

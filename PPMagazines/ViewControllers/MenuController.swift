//
//  MenuController.swift
//  PPMagazines
//
//  Created by iMac on 24.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit
import WebKit

class MenuController: UIViewController, WKNavigationDelegate {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var infoWebView: WKWebView!
    @IBOutlet weak var twitterFollow: WKWebView!
    
    @IBAction func openSupport(_ sender: Any) {
        openUrl(urlStr: "https://www.presspadapp.com/")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        infoWebView.navigationDelegate = self
        //loading twits
        let htmlPath = Bundle.main.path(forResource: "index", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let request = URLRequest(url: url)
        infoWebView.load(request)
        
    }
    //możliwość otwierania linków w safari z aplikacji
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if(navigationAction.navigationType == .linkActivated){
            if let url = navigationAction.request.url,
                let host = url.host, !host.hasPrefix("www.google.com"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
            }else{
                decisionHandler(.allow)
            }
        }else{
            decisionHandler(.allow)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // add gesture recognizer
    let closeMenu = UISwipeGestureRecognizer(target: self, action: #selector(hideMenu))
        closeMenu.direction = UISwipeGestureRecognizerDirection.left
        mainView.addGestureRecognizer(closeMenu)
    }
    
    @objc func hideMenu(){
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    //funkcja do uruchamiania linków
    func openUrl(urlStr:String!) {
        if let url = NSURL(string:urlStr) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

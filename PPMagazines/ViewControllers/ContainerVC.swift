//
//  ContainerVC.swift
//  PPMagazines
//
//  Created by iMac on 23.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {

    @IBOutlet weak var leftAlignSideMenuConstraint: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIButton!

    //zamkniecie menu po kliknieciu poza nim
    @IBAction func closeMenu(_ sender: Any) {
        toggleSideMenu()
    }
    var sideMenuOpen = false
    var showOffset: Double = 0.0   //zmienna odpowiedzialna za przesuniecie poczatku wyswietlania
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "main" && showOffset != 0.0){
            let navigationMasterController = segue.destination as? NavigationMasterController
            let magazineViewController = navigationMasterController?.viewControllers.first as? MagazineViewController
            magazineViewController?.showOffset = Double(showOffset)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        shadowView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    //jeżeli menu jest schowane to zmieniamy constraint na 0 od lewej jak chcemy je schowac to przesuwamy element za view controllera
    @objc func toggleSideMenu(){
        if(sideMenuOpen){
            shadowView.isHidden = true
            sideMenuOpen = false
            UIView.animate(withDuration: 0.3, animations: {
                self.leftAlignSideMenuConstraint.constant = -300
            })
        }else{
            shadowView.isHidden = false
            sideMenuOpen = true
            UIView.animate(withDuration: 0.3, animations: {
                self.leftAlignSideMenuConstraint.constant = 0
            })
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    

}


//
//  DetailViewController.swift
//  PPMagazines
//
//  Created by iMac on 19.01.2018.
//  Copyright © 2018 PressPad. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    //informacja otrzymywane z głównego viev controllera
    private let magazines  = Magazine.fetchMagazines()
    var indexDetail = Int()
    
    //elementy z widoku
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var gestureView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var magazineStatus: UIButton!
    @IBOutlet weak var demoStatus: UIButton!
    @IBOutlet weak var magazineBottomStatus: UIButton!
    @IBOutlet weak var demoBottomStatus: UIButton!
    @IBOutlet weak var magazineTitle: UILabel!
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    //konfigurowanie magazynu wyswietlonego
    func setupContent(indexDetail: Int) {
        //ustalenie zdjecia magazynu
        featuredImageView.sd_setImage(with: URL(string: magazines[indexDetail].cover_medium))
        //ustalanie tytulu magazynu
        magazineTitle.text = magazines[indexDetail].title
        //status button
        trashButton.isEnabled = false
        if(magazines[indexDetail].has_download){
            magazineStatus.backgroundColor = UIColor.downloadedGreen
            magazineStatus.setImage(#imageLiteral(resourceName: "check"), for: UIControlState.normal)
            magazineBottomStatus.backgroundColor = UIColor.downloadedGreen
            magazineBottomStatus.setTitle(NSLocalizedString("OPEN", comment: "opening downloaded pdf"), for: UIControlState.normal)
            trashButton.isEnabled = true
        }else if(!magazines[indexDetail].has_bought && !magazines[indexDetail].is_free){
            magazineStatus.backgroundColor = UIColor.buyRed
            magazineStatus.setImage(#imageLiteral(resourceName: "money"), for: UIControlState.normal)
            magazineBottomStatus.backgroundColor = UIColor.buyRed
            magazineBottomStatus.setTitle(NSLocalizedString("BUY", comment: "buy magazine"), for: UIControlState.normal)
        }else{
            magazineStatus.backgroundColor = UIColor.downloadBlue
            magazineStatus.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
            magazineBottomStatus.backgroundColor = UIColor.downloadBlue
            magazineBottomStatus.setTitle(NSLocalizedString("DOWNLOAD", comment: "download magazine on your device"), for: UIControlState.normal)
        }
        //demo button
        demoStatus.isHidden = false
        demoBottomStatus.isHidden = false
        demoBottomStatus.setTitle(NSLocalizedString("PREVIEW", comment: "preview pdf on your device"), for: UIControlState.normal)
        if(!magazines[indexDetail].has_demo){
            demoStatus.isHidden = true
            demoBottomStatus.isHidden = true
        }
        if(indexDetail == 0){
            leftButton.isHidden = true
        }else{
            leftButton.isHidden = false
        }
        if(indexDetail == magazines.count-1){
            rightButton.isHidden = true
        }else{
            rightButton.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //zmiana zawartosci
        setupContent(indexDetail: indexDetail)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //dodanie funkcjonalnosci guzikow
        leftButton.addTarget(self, action: #selector(leftAction), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(rightAction), for: .touchUpInside)
        //dodanie cienia
        featuredImageView.layer.shadowRadius = 1
        featuredImageView.layer.shadowOpacity = 0.8
        featuredImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        //dodanie przesuwania
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftAction))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        gestureView.addGestureRecognizer(rightSwipe)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(rightAction))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        gestureView.addGestureRecognizer(leftSwipe)
    }
    //przygotowanie powrotu do master view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "DetailToMaster"){
            let containerVC = segue.destination as? ContainerVC
            containerVC?.showOffset = Double(indexDetail)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func leftAction(sender: UIButton!){
        if(indexDetail != 0){
            indexDetail = indexDetail-1
            setupContent(indexDetail: indexDetail)
        }
    }

    @objc func rightAction(sender: UIButton!){
        if(indexDetail != magazines.count-1){
            indexDetail = indexDetail+1
            setupContent(indexDetail: indexDetail)
        }
    }
}

